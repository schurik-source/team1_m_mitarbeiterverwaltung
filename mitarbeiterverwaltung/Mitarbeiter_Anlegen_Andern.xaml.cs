﻿using klassen_Bibliothek;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;

using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;


namespace mitarbeiterverwaltung
{
    
    public partial class Mitarbeiter_Anlegen_Andern : Window
    {
        
        int index;
        string selectIndex;
        // Mitarbeiter in DB neu anlegen
        public Mitarbeiter_Anlegen_Andern(string selectrow, int Index)
        {

            //this.Mitarbeiterliste = Mitarbeiterliste;
            this.index = Index;
            selectIndex = selectrow;
            InitializeComponent();

            CB_stellung.ItemsSource = Enum.GetValues(typeof(Position));
            CB_abteilung.ItemsSource = Enum.GetValues(typeof(Abteilung));

            if (index == -1)
            {
                //Neu Anlegen
                this.Title = "Neuer Mitarbeiter";
                BT_speichern.Content = "Anlegen";
                LBL_MaID.Visibility = Visibility.Hidden;
                
            }

            //Mitarbeiterdaten aus der DB holen und in der Tabelle anzeigen
            else
            {
                MySqlConnection con = new MySqlConnection("Server=localhost;Database=Team1;Uid=root;");
                con.Open();

                string sqlquery =$"SELECT t_mitarbeiter.*,t_kontaktdaten.*, t_adresse.*, t_bankverbindung.*" +
                " FROM t_mitarbeiter INNER JOIN t_kontaktdaten ON t_mitarbeiter.KontaktdatenID = t_kontaktdaten.KontaktdatenID " +
                "INNER JOIN t_adresse ON t_kontaktdaten.AdressenID = t_adresse.AdressenID INNER JOIN t_bankverbindung " +
                $"on t_mitarbeiter.Kontonummer = t_bankverbindung.Kontonummer WHERE t_mitarbeiter.MitarbeiterID ='{selectIndex}'";

                MySqlCommand mitarbeiter = new MySqlCommand(sqlquery, con);
                var reader = mitarbeiter.ExecuteReader();

                while (reader.Read())
                {
                    //reader3
                    TB_maID.Text = reader[0].ToString();
                    TB_Vorname.Text = reader[1].ToString();
                    TB_NName.Text = reader[2].ToString();
                    DP_geburtsdatum.SelectedDate = reader.GetDateTime(reader.GetOrdinal("Geburtsdatum"));
                    string position = reader[4].ToString();
                    Enum.TryParse<Position>(position, out Position p);
                    CB_stellung.SelectedItem = p;
                    string abteilung = reader[5].ToString();
                    Enum.TryParse<Abteilung>(abteilung, out Abteilung a);
                    CB_abteilung.SelectedItem = a;
                    DP_eintrittsdatum.SelectedDate = Convert.ToDateTime(reader[6]);
                    TB_Gehalt.Text = reader[7].ToString();
                    TB_Kontonummer.Text = reader[8].ToString();
                    TB_Email.Text = reader[12].ToString();

                    TB_Telefon.Text = reader[13].ToString();
                    TB_Strasse.Text = reader[15].ToString();
                    TB_HSN.Text = reader[16].ToString();
                    TB_PLZ.Text = reader[17].ToString();
                    TB_Ort.Text = reader[18].ToString();
                    TB_Kontonummer.Text = reader[19].ToString();
                    TB_BLZ.Text = reader[20].ToString();
                    TB_Bankname.Text = reader[21].ToString();
                }


                this.Title = "Mitarbeiterdaten ändern";
                BT_speichern.Content = "Daten ändern";
            }

        }
        // eine Methode zum Mitarbeiter Anlegen, Ändern und Löschen
        public void AUD(string sqlanfrage, int status)
        {
            string massage = "";
            MySqlConnection con = new MySqlConnection("Server=localhost;Database=Team1;Uid=root;");
            con.Open();
            MySqlCommand com = con.CreateCommand();
            com.CommandText = sqlanfrage;
            
            DateTime startdatum = (DateTime)DP_eintrittsdatum.SelectedDate;
            var sqlFormattedDate = $"{startdatum.Year}-{startdatum.Month}-{startdatum.Day}";

            DateTime startdatum1 = (DateTime)DP_geburtsdatum.SelectedDate;
            var Gebdatum = $"{startdatum1.Year}-{startdatum1.Month}-{startdatum1.Day}";

            switch (status)
            {
                case 0:
                    // neuen Mitarbeiter anlegen
                    com.Parameters.AddWithValue("@MitarbeiterID", selectIndex);
                    com.Parameters.AddWithValue("@Vorname", TB_Vorname.Text);
                    com.Parameters.AddWithValue("@Nachname", TB_NName.Text);
                    com.Parameters.AddWithValue("@Geburtsdatum", Gebdatum);
                    com.Parameters.AddWithValue("@Position", Convert.ToString(CB_stellung.SelectedItem));
                    com.Parameters.AddWithValue("@Abteilung", Convert.ToString(CB_abteilung.SelectedItem));
                    com.Parameters.AddWithValue("@Eintrittsdatum", sqlFormattedDate);
                    com.Parameters.AddWithValue("@Gehalt", Convert.ToDecimal(TB_Gehalt.Text));
                    com.Parameters.AddWithValue("@Strasse", TB_Strasse.Text);
                    com.Parameters.AddWithValue("@Hausnummer", TB_HSN.Text);
                    com.Parameters.AddWithValue("@PLZ", TB_PLZ.Text);
                    com.Parameters.AddWithValue("@Ort", TB_Ort.Text);
                    com.Parameters.AddWithValue("@Email", TB_Email.Text);
                    com.Parameters.AddWithValue("@Telefon", TB_Telefon.Text);
                    com.Parameters.AddWithValue("@Kontonummer", TB_Kontonummer.Text);
                    com.Parameters.AddWithValue("@BLZ", TB_BLZ.Text);
                    com.Parameters.AddWithValue("@Bankname", TB_Bankname.Text);
                                        
                    com.ExecuteNonQuery();

                    break;

                //Mitarbeiterdaten ändern
                case 1:
                    com.Parameters.AddWithValue("@MitarbeiterID", selectIndex);
                    com.Parameters.AddWithValue("@Vorname", TB_Vorname.Text);
                    com.Parameters.AddWithValue("@Nachname", TB_NName.Text);
                    com.Parameters.AddWithValue("@Geburtsdatum", Gebdatum);
                    com.Parameters.AddWithValue("@Position", Convert.ToString(CB_stellung.SelectedItem));
                    com.Parameters.AddWithValue("@Abteilung", Convert.ToString(CB_abteilung.SelectedItem));
                    com.Parameters.AddWithValue("@Eintrittsdatum", sqlFormattedDate);
                    com.Parameters.AddWithValue("@Gehalt", Convert.ToDecimal(TB_Gehalt.Text));
                    com.Parameters.AddWithValue("@Strasse", TB_Strasse.Text);
                    com.Parameters.AddWithValue("@Hausnummer", TB_HSN.Text);
                    com.Parameters.AddWithValue("@PLZ", TB_PLZ.Text);
                    com.Parameters.AddWithValue("@Ort", TB_Ort.Text);
                    com.Parameters.AddWithValue("@Email", TB_Email.Text);
                    com.Parameters.AddWithValue("@Telefon", TB_Telefon.Text);
                    com.Parameters.AddWithValue("@Kontonummer", TB_Kontonummer.Text);
                    com.Parameters.AddWithValue("@BLZ", TB_BLZ.Text);
                    com.Parameters.AddWithValue("@Bankname", TB_Bankname.Text);

                    com.ExecuteNonQuery();
                    massage = "updated";
                    MessageBox.Show(massage);
                    break;
                // Mitarbeiter aus der Liste löschen
                case 2:
                    com.Parameters.AddWithValue("@MitarbeiterID", selectIndex);
                    com.ExecuteNonQuery();
                    massage = "Mitarbeiter gelöscht !";
                    MessageBox.Show(massage);
                    break;

                default:
                    break;
            }

            con.Close();
        }
        // Überürüfen ob Kontonummer in DB schon vorhanden ist
        public bool Kontonum_check()
        {
            bool result = true;

            MySqlConnection conect = new MySqlConnection("Server=localhost;Database=Team1;Uid=root;");
            conect.Open();

            string kn_sqlquery = "SELECT  Kontonummer FROM  t_bankverbindung ";

            MySqlCommand knumcom = new MySqlCommand(kn_sqlquery, conect);
            var reader = knumcom.ExecuteReader();

            while (reader.Read())
            {
                if (TB_Kontonummer.Text == reader[0].ToString())
                {
                    result = false;
                    MessageBox.Show("Kontonummer bereits vorhanden !");
                }
            }
            return result;
        }

        //MA in DB anlegen
        private void BT_speichern_Click(object sender, RoutedEventArgs e)
        {
            if (index == -1)
            {
                string sql = "insert into t_mitarbeiter (MitarbeiterID,Vorname,Nachname,Geburtsdatum,Position,Abteilung,Eintrittsdatum,Gehalt,Kontonummer) values (@MitarbeiterID,@Vorname,@Nachname,@Geburtsdatum,@Position,@Abteilung,@Eintrittsdatum,@Gehalt,@Kontonummer)";
                string sql1 = "insert into t_adresse (Strasse,Hausnummer,PLZ,Ort) values (@Strasse,@Hausnummer,@PLZ,@Ort)";
                string sql2 = "insert into t_kontaktdaten (Email,Telefon) values (@Email,@Telefon)";
                string sql3 = "insert into t_bankverbindung (Kontonummer,BLZ,Bankname) values (@Kontonummer,@BLZ,@Bankname)";
                
                this.AUD(sql1, 0);
                this.AUD(sql2, 0);
                this.AUD(sql3, 0);
                this.AUD(sql, 0);

                MySqlConnection conect = new MySqlConnection("Server=localhost;Database=Team1;Uid=root");
                conect.Open();
                string sqlquery = "SELECT * FROM t_kontaktdaten";
                MySqlCommand sqlcommand = new MySqlCommand(sqlquery, conect);

                int tempvalue=0;
                var reader = sqlcommand.ExecuteReader();
                while (reader.Read())
                {
                    if((int)reader[0]   > tempvalue)
                    {
                        tempvalue = (int)reader[0];
                    }
                }
                reader.Close();

                //sqlcommand = new MySqlCommand("", conect);

                string upd_Kon_ID = $"update t_mitarbeiter set KontaktdatenID= {tempvalue} where KontaktdatenID is null";
                MySqlCommand comman_upt = new MySqlCommand(upd_Kon_ID, conect);
                comman_upt.ExecuteNonQuery();

                string sqlquery2 = "SELECT * FROM t_adresse";

                MySqlCommand sqlcommand2 = new MySqlCommand(sqlquery2, conect);

                int tempvalue2 = 0;
                var reader2 = sqlcommand2.ExecuteReader();
                while (reader2.Read())
                {
                    if ((int)reader2[0] > tempvalue2)
                    {
                        tempvalue2 = (int)reader2[0];
                    }
                }
                reader2.Close();
               
                string upd_adr_ID = $"update t_kontaktdaten set AdressenID = {tempvalue2} where AdressenID is null";
                MySqlCommand command_upt = new MySqlCommand(upd_adr_ID, conect);
                command_upt.ExecuteNonQuery();
                conect.Close();

            }
            else
            {
                //MA daten updaten in allen Tabellen

                string sql = "update t_mitarbeiter m ,t_kontaktdaten k ,t_adresse a, t_bankverbindung b set m.Vorname = @Vorname," +
                    "m.Nachname = @Nachname," +
                    "m.Geburtsdatum = @Geburtsdatum," +
                    "m.Position = @Position ," +
                    "m.Abteilung = @Abteilung," +
                    "m.Eintrittsdatum = @Eintrittsdatum," +
                    "m.Gehalt = @Gehalt, " +
                    "k.Email = @Email, " +
                    "a.Strasse = @Strasse, " +
                    "k.Telefon = @Telefon, " +
                    "b.Kontonummer = @Kontonummer, " +
                    "b.BLZ = @BLZ " +
                    "where  m.KontaktdatenID= k.KontaktdatenID and k.AdressenID = a.AdressenID and m.Kontonummer = b.Kontonummer and MitarbeiterID = " + selectIndex + "";

                this.AUD(sql, 1);
            }
            this.Close();
        }
                
        private void TB_Kontonummer_KeyUp(object sender, KeyEventArgs e)
        {
            if (Kontonum_check() == false && index == -1) TB_Kontonummer.Text = null;
        }

    }
}
