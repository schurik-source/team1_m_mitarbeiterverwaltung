﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace mitarbeiterverwaltung
{
    /// <summary>
    /// Interaktionslogik für nachrichten_liste.xaml
    /// </summary>
    public partial class nachrichten_liste : Window
    {
        string selectIndex;

        //Mitarbeiter Nachrichten aus DB auslesen und in DataGrid anzeigen
        public nachrichten_liste(string selectrow)
        {
            InitializeComponent();

            selectIndex = selectrow;

            MySqlConnection conect = new MySqlConnection("Server=localhost;Database=Team1;Uid=root");
            conect.Open();

            string listquery = $"SELECT * FROM t_memo where AbsenderID='{selectIndex}'";
            
            MySqlCommand comman = new MySqlCommand(listquery, conect);

            DataTable dt = new DataTable();
            MySqlDataAdapter da = new MySqlDataAdapter(comman);
            da.Fill(dt);

            DG_Memo.ItemsSource = dt.DefaultView;


            conect.Close();
        }
    }
}
