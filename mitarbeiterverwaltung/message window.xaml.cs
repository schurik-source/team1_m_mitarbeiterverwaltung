﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace mitarbeiterverwaltung
{
    /// <summary>
    /// Interaktionslogik für message_window.xaml
    /// </summary>
    public partial class message_window : Window
    {
        string selectIndex;
        public message_window(string selectrow)
        {
            InitializeComponent();

            selectIndex = selectrow;
            txb_empID.Text = selectIndex;
            
        }
        //Memo speichern in der Datenbank       
        private void Button_Nachticht_senden_Click(object sender, RoutedEventArgs e)
        {
            MySqlConnection conect = new MySqlConnection("Server=localhost;Database=Team1;Uid=root");
            conect.Open();
            var datumtoday = $"{DateTime.Now.Year}-{DateTime.Now.Month}-{DateTime.Now.Day}";
            
            string sqlwritequery = "insert into t_memo (AbsenderID,EmpfaengerID,Versendezeit,Text)" +
                $"values({Convert.ToInt32(txb_absID.Text)},{Convert.ToInt32(selectIndex)},'{datumtoday}','{txt_input.Text}') ";

            MySqlCommand command = new MySqlCommand(sqlwritequery, conect);

            command.ExecuteNonQuery();

            conect.Close();

            this.Close();
        }
    }
}
