﻿using klassen_Bibliothek;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace mitarbeiterverwaltung
{
    /// <summary>
    /// Interaktionslogik für MainWindow.xaml
    /// </summary>
    
    public partial class MainWindow : Window
    {
        
        string chang_rowIndex;
        
        //Datagrid Ansichtupdate 
        void DatagridUpdate()
        {
            MySqlConnection conect = new MySqlConnection("Server=localhost;Database=Team1;Uid=root");
            conect.Open();
            string sqlquery = "SELECT t_mitarbeiter.*,t_kontaktdaten.*, t_adresse.*, t_bankverbindung.*" +
                " FROM t_mitarbeiter INNER JOIN t_kontaktdaten ON t_mitarbeiter.KontaktdatenID = t_kontaktdaten.KontaktdatenID " +
                "INNER JOIN t_adresse ON t_kontaktdaten.AdressenID = t_adresse.AdressenID INNER JOIN t_bankverbindung " +
                "on t_mitarbeiter.Kontonummer = t_bankverbindung.Kontonummer";
            //WHERE t_mitarbeiter.MitarbeiterID=6"
            MySqlCommand comman = new MySqlCommand(sqlquery, conect);

            DataTable dt = new DataTable();
            MySqlDataAdapter da = new MySqlDataAdapter(comman);
            da.Fill(dt);
            
            Dg_Mitarbeiter.ItemsSource = dt.DefaultView;
            

            conect.Close();
            
        }
        public MainWindow()
        {
            InitializeComponent();

            DatagridUpdate();
        }
       
        private void Mitarbeiter_anlegen(object sender, RoutedEventArgs e)
        {
            Mitarbeiter_Anlegen_Andern maaf = new Mitarbeiter_Anlegen_Andern(chang_rowIndex, -1);
            maaf.ShowDialog();
            DatagridUpdate();
        }

        private void Button_berarbeiten(object sender, RoutedEventArgs e)
        {
            Mitarbeiter_Anlegen_Andern maaf = new Mitarbeiter_Anlegen_Andern(chang_rowIndex, Dg_Mitarbeiter.SelectedIndex);
                       
            maaf.ShowDialog();
            DatagridUpdate();
        }

        private void Dg_Mitarbeiter_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            DataRowView row = (DataRowView)Dg_Mitarbeiter.SelectedItem;
            if(row != null)
            {
                chang_rowIndex = row["MitarbeiterID"].ToString();

            }
        }

        private void Button_Mitrabeiter_loschen_Click(object sender, RoutedEventArgs e)
        {
            string sqlString = "delete from t_mitarbeiter where MitarbeiterID= @MitarbeiterID";
            Mitarbeiter_Anlegen_Andern malf = new Mitarbeiter_Anlegen_Andern(chang_rowIndex, Dg_Mitarbeiter.SelectedIndex);
            malf.AUD(sqlString,2);
            DatagridUpdate();
        }

        private void Button_zum_Hauptmenu_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void Button_Nachricht_schreibenClick(object sender, RoutedEventArgs e)
        {
            if(chang_rowIndex != null)
            {

                message_window mess = new message_window(chang_rowIndex);
                mess.ShowDialog();
            }
            else
            {
                MessageBox.Show("kein ID ausgewählt !");
            }
           
            
        }

        private void Button_Nachricht_lesen_Click(object sender, RoutedEventArgs e)
        {
            if (chang_rowIndex != null)
            {

                nachrichten_liste mesl = new nachrichten_liste(chang_rowIndex);
                mesl.ShowDialog();
            }
            else
            {
                MessageBox.Show("kein ID ausgewählt !");
            }
        }
    }
}
